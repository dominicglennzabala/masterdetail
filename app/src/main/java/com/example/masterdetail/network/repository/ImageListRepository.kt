package com.example.masterdetail.network.repository

import androidx.paging.PagingData
import com.example.masterdetail.data.model.ImageListItem
import kotlinx.coroutines.flow.Flow

interface ImageListRepository {
    suspend fun getImages(): Flow<PagingData<ImageListItem>>
}