package com.example.masterdetail.network.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.masterdetail.data.ImageListRemoteMediator
import com.example.masterdetail.data.db.ImageListDb
import com.example.masterdetail.data.model.ImageListItem
import com.example.masterdetail.network.PicsumApi
import com.example.masterdetail.utils.Constants.Companion.QUERY_PER_PAGE
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ExperimentalPagingApi
class ImageListRepositoryImpl @Inject constructor(
    private val picsumApi: PicsumApi,
    private val imageListDb: ImageListDb
) : ImageListRepository {

    override suspend fun getImages(): Flow<PagingData<ImageListItem>> {
        val pagingSourceLocal = { imageListDb.getImageListDao().getAllImages() }
        return Pager(
            config = PagingConfig(pageSize = QUERY_PER_PAGE, prefetchDistance = 2),
            remoteMediator = ImageListRemoteMediator(picsumApi, imageListDb),
            pagingSourceFactory = pagingSourceLocal
        ).flow
    }
}