package com.example.masterdetail.di

import android.content.Context
import androidx.paging.ExperimentalPagingApi
import com.example.masterdetail.data.db.ImageListDb
import com.example.masterdetail.network.PicsumApi
import com.example.masterdetail.network.repository.ImageListRepositoryImpl
import com.example.masterdetail.utils.Constants.Companion.BASE_URL
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
@OptIn(ExperimentalPagingApi::class)
object ApplicationModule {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .baseUrl(BASE_URL)
            .build()

    @Provides
    @Singleton
    fun providePicsumApi(retrofit: Retrofit): PicsumApi = retrofit.create(PicsumApi::class.java)

    @Singleton
    @Provides
    fun provideImageListDb(@ApplicationContext appContext: Context) =
        ImageListDb.getDatabase(appContext)


    @Singleton
    @Provides
    fun provideRepository(
        picsumApi: PicsumApi,
        imageListDb: ImageListDb
    ) = ImageListRepositoryImpl(picsumApi, imageListDb)
}