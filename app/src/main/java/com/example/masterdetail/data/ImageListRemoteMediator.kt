package com.example.masterdetail.data

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.example.masterdetail.data.db.ImageListDb
import com.example.masterdetail.data.model.ImageListItem
import com.example.masterdetail.data.model.PageKey
import com.example.masterdetail.network.PicsumApi
import com.example.masterdetail.utils.Constants.Companion.DEFAULT_PAGE_INDEX

@OptIn(ExperimentalPagingApi::class)
class ImageListRemoteMediator(private val service: PicsumApi, val db: ImageListDb) :
    RemoteMediator<Int, ImageListItem>() {
    private val imageListDao = db.getImageListDao()
    private val pageKeyDao = db.pageKeyDao()

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, ImageListItem>
    ): MediatorResult {
        val page = when (loadType) {
            LoadType.REFRESH -> {
                val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                remoteKeys?.nextKey?.minus(1) ?: DEFAULT_PAGE_INDEX
            }
            LoadType.PREPEND -> {
                val remoteKeys = getRemoteKeyForFirstItem(state)

                val prevKey = remoteKeys?.prevKey
                    ?: return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
                prevKey
            }
            LoadType.APPEND -> {
                val remoteKeys = getRemoteKeyForLastItem(state)

                val nextKey = remoteKeys?.nextKey
                    ?: return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
                nextKey
            }
        }

        try {

            val apiResponse = service.getImages(page)
            val resBody = apiResponse.body()
            val images = resBody
            var endOfPaginationReached = false
            images?.let {
                endOfPaginationReached = it.isEmpty()
            }
            db.withTransaction {
                if (loadType == LoadType.REFRESH) {
                    pageKeyDao.clearAll()
                    imageListDao.deleteAllImages()
                }
                val prevKey = if (page == DEFAULT_PAGE_INDEX) null else page - 1
                val nextKey = if (endOfPaginationReached) null else page + 1
                val keys = images?.map {
                    PageKey(id = it.id, prevKey = prevKey, nextKey = nextKey)
                }
                if (keys != null) {
                    pageKeyDao.insertAll(keys)
                }
                if (images != null) {
                    imageListDao.insertAll(images)
                }
            }
            return MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
        } catch (e: Exception) {
            return MediatorResult.Error(e)
        }
    }

    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, ImageListItem>): PageKey? {
        return state.pages.lastOrNull() { it.data.isNotEmpty() }?.data?.lastOrNull()
            ?.let { item ->
                pageKeyDao.getNextPageKey(item.id)
            }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, ImageListItem>): PageKey? {
        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()
            ?.let { item ->
                pageKeyDao.getNextPageKey(item.id)
            }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(
        state: PagingState<Int, ImageListItem>
    ): PageKey? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { id ->
                pageKeyDao.getNextPageKey(id)
            }
        }
    }
}