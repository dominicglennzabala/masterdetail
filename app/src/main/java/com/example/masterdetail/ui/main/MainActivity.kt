package com.example.masterdetail.ui.main

import android.content.Intent
import android.widget.Toast
import androidx.activity.viewModels
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.masterdetail.base.BaseActivity
import com.example.masterdetail.data.model.ImageListItem
import com.example.masterdetail.databinding.ActivityMainBinding
import com.example.masterdetail.databinding.ItemImageBinding
import com.example.masterdetail.ui.detail.FullScreenActivity
import com.example.masterdetail.ui.main.adapter.ImageAdapter
import com.example.masterdetail.ui.main.adapter.PagingLoadStateAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import javax.inject.Inject

@AndroidEntryPoint
@ExperimentalPagingApi
class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>(),
    ImageAdapter.ImageItemClickListener {

    private val mainViewModel: MainViewModel by viewModels()

    @Inject
    lateinit var imageAdapter: ImageAdapter

    override fun getVM(): MainViewModel = mainViewModel

    override fun bindVM(binding: ActivityMainBinding, vm: MainViewModel) =
        with(binding) {
            val rLayoutManager =
                LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)

            with(imageAdapter) {
                rvImages.apply {
                    postponeEnterTransition()
                    viewTreeObserver.addOnPreDrawListener {
                        startPostponedEnterTransition()
                        true
                    }
                    layoutManager = rLayoutManager
                }
                rvImages.adapter = withLoadStateHeaderAndFooter(
                    header = PagingLoadStateAdapter(this),
                    footer = PagingLoadStateAdapter(this)
                )

                swipeRefreshLayout.setOnRefreshListener { refresh() }
                imageClickListener = this@MainActivity

                with(vm) {
                    launchOnLifecycleScope {
                        imageResponse.collectLatest { submitData(it) }
                    }
                    launchOnLifecycleScope {
                        loadStateFlow.collectLatest {
                            swipeRefreshLayout.isRefreshing = it.refresh is LoadState.Loading
                        }
                    }
                }

                launchOnLifecycleScope {
                    imageAdapter.loadStateFlow.collect {
                        val errorState = it.source.append as? LoadState.Error
                            ?: it.source.prepend as? LoadState.Error
                            ?: it.append as? LoadState.Error
                            ?: it.prepend as? LoadState.Error
                            ?: it.source.refresh as? LoadState.Error
                        errorState?.let {
                            Toast.makeText(
                                this@MainActivity,
                                "\uD83D\uDE28 Wooops ${it.error}",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            }
        }


    override fun setBinding(): ActivityMainBinding = ActivityMainBinding.inflate(layoutInflater)

    override fun onImageClicked(binding: ItemImageBinding, item: ImageListItem) {
        val intent = Intent(this@MainActivity, FullScreenActivity::class.java)
        intent.putExtra("item", item)
        startActivity(intent)
    }
}