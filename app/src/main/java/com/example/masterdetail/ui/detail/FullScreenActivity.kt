package com.example.masterdetail.ui.detail

import android.os.Bundle
import androidx.activity.viewModels
import com.bumptech.glide.Glide
import com.example.masterdetail.R
import com.example.masterdetail.base.BaseActivity
import com.example.masterdetail.data.model.ImageListItem
import com.example.masterdetail.databinding.ActivityDetailBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FullScreenActivity : BaseActivity<ActivityDetailBinding, FullScreenViewModel>() {

    private val TAG = "FullScreenActivity"
    private val fullScreenViewModel: FullScreenViewModel by viewModels()
    private lateinit var imageItem: ImageListItem


    override fun onViewReady(savedInstanceState: Bundle?) {
        super.onViewReady(savedInstanceState)
        val bundle = intent.extras
        if (bundle != null) {
            val item = bundle.getSerializable("item")
            imageItem = item as ImageListItem
            setupUI()

        }
        savedInstanceState?.let {
            viewModel.hideErrorToast()
        }
    }

    override fun setBinding(): ActivityDetailBinding = ActivityDetailBinding.inflate(layoutInflater)

    override fun getVM(): FullScreenViewModel = fullScreenViewModel

    override fun bindVM(binding: ActivityDetailBinding, vm: FullScreenViewModel) = Unit

    private fun setupUI() {
        with(binding) {
            txtAuthor.text = imageItem.author

            Glide.with(this@FullScreenActivity)
                .load(imageItem.download_url)
                .placeholder(R.drawable.placeholder)
                .into(imageFullScreen)


        }
    }


}