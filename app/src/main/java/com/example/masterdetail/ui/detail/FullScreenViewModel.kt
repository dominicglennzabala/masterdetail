package com.example.masterdetail.ui.detail

import androidx.lifecycle.MutableLiveData
import com.example.masterdetail.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class FullScreenViewModel @Inject constructor() : BaseViewModel() {

    private val TAG = "FullScreenViewModel"
    private val _errorToast = MutableLiveData<String>()


    fun hideErrorToast() {
        _errorToast.postValue("")
    }

    private fun onError(throwable: Throwable) {
        throwable.message?.let {
            _errorToast.postValue(it)
        }
    }
}